#!/usr/bin/env groovy
def call(){
    pipeline{
        agent any
        stages {
            stage('Sonarqube') {
                environment {
                    SCANNER_HOME = tool 'SonarQubeScanner'
                }
                steps {
                    withSonarQubeEnv('sonarqube') {
                        sh" ${SCANNER_HOME}/bin/sonar-scanner \
                            -Dsonar.projectKey=Trygon \
                            -Dsonar.sources=. "
                    }
                    timeout(time: 5, unit: 'MINUTES') {
                        waitForQualityGate abortPipeline: true
                    }
                }
            }
        }
    }
}